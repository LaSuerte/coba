package org.prepareUAS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrepareUasApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrepareUasApplication.class, args);
	}
}
