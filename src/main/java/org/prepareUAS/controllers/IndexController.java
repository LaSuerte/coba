package org.prepareUAS.controllers;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.prepareUAS.dao.RuteDao;
import org.prepareUAS.dao.TransaksiDao;
import org.prepareUAS.dao.UserDao;
import org.prepareUAS.models.Rute;
import org.prepareUAS.models.Transaksi;
import org.prepareUAS.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class IndexController {
	@Autowired
	private UserDao uDao;

	@Autowired
	private RuteDao rDao;
	@Autowired
	private TransaksiDao tDao;

	@RequestMapping("/")
	public String index() {
		return "index";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(HttpServletRequest request) {
		String username = request.getParameter("user");
		String password = request.getParameter("pass");
		if (username.equals("admin") && password.equals("admin")) {
			return "redirect:/admin";
		} else {
			User userLogin = uDao.loginRegister(username, password);
			request.getSession().setAttribute("userLogin", userLogin);
		}
		return "redirect:/user";
	}

	@RequestMapping("/user")
	public String user(Model model, HttpServletRequest request) {
		model.addAttribute("transaksi", new Transaksi());
		model.addAttribute("allRute", rDao.getAllRute());
		model.addAttribute("allTransaksi",
				tDao.getAllTransaksiByIdPemesan(((User) request.getSession().getAttribute("userLogin")).getId()));
		return "homeUser";
	}

	@RequestMapping("/admin")
	public String admin(Model model) {
		model.addAttribute("rute", new Rute());
		model.addAttribute("allTransaksi", tDao.getAllTransaksiByStatus("Belum Lunas"));
		return "homeAdmin";
	}

	@RequestMapping(value = "/addRute", method = RequestMethod.POST)
	public String addRute(Model model, Rute rute) {
		model.addAttribute("rute", rDao.save(rute));
		return "redirect:/admin";
	}

	@RequestMapping(value = "/addTransaksi", method = RequestMethod.POST)
	public String addTransaksi(Model model, Transaksi transaksi, HttpServletRequest request) {
		transaksi.setId_pemesan(((User) request.getSession().getAttribute("userLogin")).getId());
		transaksi.setStatus("Belum Lunas");
		if (transaksi.getTotal() > 0) {
			model.addAttribute("transaksi", tDao.save(transaksi));
		}

		return "redirect:/user";
	}

	@RequestMapping("/confirm/{id}")
	public String confirm(@PathVariable int id) {
		tDao.confirmById(id);
		return "redirect:/admin";
	}
}
