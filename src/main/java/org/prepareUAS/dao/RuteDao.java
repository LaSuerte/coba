package org.prepareUAS.dao;

import java.util.List;

import org.prepareUAS.models.Rute;

public interface RuteDao {
	Rute save(Rute rute);
	List<Rute> getAllRute();
}
