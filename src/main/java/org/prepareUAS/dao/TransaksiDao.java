package org.prepareUAS.dao;

import java.util.List;

import org.prepareUAS.models.Transaksi;

public interface TransaksiDao {
	List<Transaksi> getAllTransaksiByIdPemesan(int id);
	List<Transaksi> getAllTransaksiByStatus(String status);
	Transaksi save(Transaksi transaksi);
	void confirmById(int id);
	

}
