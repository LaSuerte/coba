package org.prepareUAS.dao;

import java.util.List;

import org.prepareUAS.models.User;

public interface UserDao {
	List<User> getAllUser();
	User loginRegister(String username, String password);
}
