package org.prepareUAS.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.prepareUAS.dao.RuteDao;
import org.prepareUAS.models.Rute;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RuteDaoImpl implements RuteDao {
	@Autowired
	private EntityManagerFactory emf;

	@Override
	public List<Rute> getAllRute() {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from Rute", Rute.class).getResultList();
	}

	@Override
	public Rute save(Rute rute) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Rute saved = em.merge(rute);
		em.getTransaction().commit();
		return saved;
	}

}
