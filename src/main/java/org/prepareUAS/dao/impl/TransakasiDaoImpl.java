package org.prepareUAS.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.prepareUAS.dao.TransaksiDao;
import org.prepareUAS.models.Transaksi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransakasiDaoImpl implements TransaksiDao {
	@Autowired
	private EntityManagerFactory emf;

	@Override
	public List<Transaksi> getAllTransaksiByIdPemesan(int id) {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from Transaksi where id_pemesan=" + id, Transaksi.class).getResultList();
	}

	@Override
	public Transaksi save(Transaksi transaksi) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Transaksi saved = em.merge(transaksi);
		em.getTransaction().commit();
		return saved;
	}

	@Override
	public List<Transaksi> getAllTransaksiByStatus(String status) {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from Transaksi where status='" + status + "'", Transaksi.class).getResultList();
	}

	@Override
	public void confirmById(int id) {
		EntityManager em = emf.createEntityManager();
		Transaksi t = em.find(Transaksi.class, id);
		t.setStatus("Lunas");
		t = save(t);

	}

}
