package org.prepareUAS.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.prepareUAS.dao.UserDao;
import org.prepareUAS.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserDaoImpl implements UserDao {
	@Autowired
	private EntityManagerFactory emf;

	@Override
	public List<User> getAllUser() {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from User", User.class).getResultList();
	}

	@Override
	public User loginRegister(String username, String password) {
		
		for (User u : getAllUser()) {
			if(u.getUsername().equals(username) && u.getPassword().equals(password)){
				return u;
			}
		}
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		User saved = new User();
		saved.setUsername(username);
		saved.setPassword(password);
		saved = em.merge(saved);
		em.getTransaction().commit();
		return saved;
	}

}
