package org.prepareUAS.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Transaksi {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private int id_pemesan;
	private String rute;
	private int total;
	private String status;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getId_pemesan() {
		return id_pemesan;
	}
	public void setId_pemesan(int id_pemesan) {
		this.id_pemesan = id_pemesan;
	}
	public String getRute() {
		return rute;
	}
	public void setRute(String rute) {
		this.rute = rute;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
